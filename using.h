//
// Created by Matej Peluha on 4/5/2019.
//

#ifndef ZADANIE8_USING_H
#define ZADANIE8_USING_H


#include <math.h>
#include "generate.h"
#include "read_and_write.h"

#define END "end"
#define HELP "help"
#define SHOW "show"
#define SORT "sort"
#define ADD "add"
#define CHANGE "change"
#define DELETING "delete"
#define ESCAPE_NUM 1000
#define ESCAPE_STR "1000"
#define SYNTAX_ERROR -1



#define MAX_INPUT_SIZE 100
#define MINIMUM_SALARY 1000

void use_database(Employee *all_employees, int num_emp, FILE *fptr);

int string_is_number(char emp_to_show[], int len);
int string_convert_to_number(char emp_to_show[],int len);
int strings_are_same(char *input, char string[]);

#endif //ZADANIE8_USING_H
