#include "generate.h"
#include "using.h"
#include "read_and_write.h"

//AHOOOOOOOOOOOOOOj

int main() {

    srand(time(NULL));  //inicializacia randomu

    int num_emp=0;    //pocet zamestnancov

    int error=0;

    Employee *all_employees;   //buduce pole vsetkych  zamestnancov

    FILE *fptr; //pointer na textovy subor v ktorom je

    fptr=fopen("database.txt","r+");    //subor v ktorom je databaza otvorime

    CLEAR_SCREEN();

    //subor neexistuje
    if(NULL==fptr){
        printf("Subor neexistuje.");
    }

    //subor existuje
    else {

        //subor je prazdny
        if (database_is_empty(fptr)) {

            num_emp = get_number_employees(); //uzivatel zada pocet generovanych zamestnancov

            all_employees=(Employee*) malloc(num_emp * sizeof(Employee));    //vytvorenie pola o velkosti ktoru sme si zadali

            generate_employees(all_employees, num_emp);  //generuje zamestnancov, posielame smernik na pole zamestnancov aby sme ich menili v pameti

            write_database(fptr,num_emp,all_employees); //zapise vygenerovanych zamestnancov do databazy

        }

        //v subore nieco je
        else {

            num_emp=number_of_employees(fptr);  //pocet zamestnancov zodpoveda poctu riadkov v databaze

            all_employees=(Employee*) malloc(num_emp* sizeof(Employee));    //vytvorime pole o velkosti poctu zamestnancov v databaze

            error=load_database(fptr, all_employees);   //nacitame databazu z .txt do pola

        }

        //pokial nenastal error pri nacitani zo subora .txt
        if (error==0) {
            use_database(all_employees, num_emp, fptr);   //pouzivanie databazy, prikazy uzivatela
        }

    }

    free(fptr);
    fclose(fptr);   //subor s databazou zatvorime

    return 0;
}