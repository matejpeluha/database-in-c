//
// Created by Matej Peluha on 4/4/2019.
//

#include "generate.h"
#include "using.h"
#include "names.h"

//uzivatel zada kolko zamestnancov chce generovat
int get_number_employees(){

    int num_emp=-1;
    char char_num_emp[50];

    do {
        CLEAR_SCREEN();
        printf("Zadaj pozadovany pocet zamestnancov na generovanie(minimalne 0, maximalne %d): ", MAX_GEN_EMPLOYEES);
        scanf(" %s", char_num_emp);

        if(string_is_number(char_num_emp,strlen(char_num_emp))==0){
            continue;
        }

        num_emp=string_convert_to_number(char_num_emp,strlen(char_num_emp));

    }while((num_emp>=MAX_GEN_EMPLOYEES || num_emp<0));
    CLEAR_SCREEN();
    return num_emp;
}



//generuje prijem
int gen_income(Employee *employee){
    int income;

    //pokial je zamestnanec manager tak ma vyssi plat
    if (strcmp(employee->position,"IT manager")==0){
        income=3500+rand()%2000;
    }

    else{
        income=1500+rand()%2000;
    }

    return income;
}



//zapise mesacny plat
void write_income(Employee *employee){
    employee->income=gen_income(employee);
}



//testuje ci je ID uz pouzite
int id_is_used(Employee all_employees[],int num_employees, int ID){
    int test=0;

    for(int loop=0;loop<num_employees;loop++){
        if(all_employees[loop].ID==ID){
            test++;
            break;
        }
    }

    return test;
}



//generuje ID
int gen_ID(int num_employees, Employee all_employees[]){

    int ID;

    do {
        ID = (rand() % 5000) + 5000;
    }while(id_is_used( all_employees, num_employees, ID));

    return ID;
}



//zapise ID
void write_ID(Employee *employee, int num_employees, Employee all_employees[]){
    employee->ID=gen_ID(num_employees, all_employees);
}



//vygeneruje nahodne cislo od 0 po 100 na urcenie toho aku poziciu ma, kazda pozicia ma danu daku sancu an zisk
int random_chance(){
    int num;

    num=rand()%101;

    return num;
}



//generuje nahodnu pracovnu poziciu podla danej sance na zisk pozicie
void gen_position(char *pos){

    int chance;

    chance=random_chance(); //vygeneruje nahodne cislo

    if (chance<=CHANCE_MANAGER){
        strcpy(pos,all_employees[0]);
    }

    else if(chance<=CHANCE_WEBDESIGNER){
        strcpy(pos,all_employees[1]);
    }

    else if(chance<=CHANCE_ANALYTIC){
        strcpy(pos,all_employees[2]);
    }

    else if(chance<=CHANCE_DEVELOPER){
        strcpy(pos,all_employees[3]);
    }

    else if(chance<=CHANCE_TESTER){
        strcpy(pos,all_employees[4]);
    }

    else if(chance<=CHANCE_ADMIN){
        strcpy(pos,all_employees[5]);
    }
}



//zapise pracovnu poziciu
void write_position(Employee *employee){

    char pos[MAX_LENGTH_POSITION];

    gen_position(pos);

    strcpy(employee->position,pos);
}



//generuje rok narodenia
int gen_year(){
    int year;

    year=1970+rand()%26;

    return year;
}



//generuje mesiac narodenia
int gen_month(){
    int month;

    month=1+rand()%12;

    return month;
}



int gen_day(int month, int year){

    int day;

    if (month==1 || month==3 | month==5 || month==7 || month==8 || month==10 || month==12){
        day=1+rand()%31;
    }

    else if(month==2){

        //priestyupny rok
        if (year%4==0){
            day=1+rand()%29;
        }

        else{
            day=1+rand()%28;
        }
    }

    else{
        day=1+rand()%30;
    }

    return day;
}



//zapis datum narodenia
void write_birth_date(Date *birth_date){

    int day,month,year;

    year=gen_year();    //vygeneruje rok narodenia
    birth_date->year=year; //zapise rok narodenia

    month=gen_month(); //vygeneruje mesiac narodenia
    birth_date->month=gen_month(); //zapise mesiac narodenia

    day=gen_day(month, year);   //generuje den narodenia
    birth_date->day=day; //zapisuje den narodenia
}



//nahodne cislo mena zo zonamu podla pohlavia
int random_name(int num_names){
    int num;

    num=rand()%(num_names);

    return num;
}



//zapis krstneho mena
void write_first_name(PersonalData *personal_data, char name[]){
    strcpy(personal_data->name,name);
    strcat(personal_data->name," ");
}



//zapis priezvyska
void write_last_name(PersonalData *personal_data, int sex, char name[]) {
    strcat(personal_data->name, name);
    if (sex == 0) {
        strcat(personal_data->name, "ova");
    }
}



//vyber mena
void choose_random_name(int sex, PersonalData *personal_data){
    int rand_name;
    char first_name[MAX_LENGTH_NAME];
    char last_name[MAX_LENGTH_NAME];

    //zeny
    if (sex==0){
        //nahodny vyber krstneho mena
        rand_name=random_name(NUM_WOMEN_NAMES);
        strcpy(first_name,women_names[rand_name]);

        write_first_name(personal_data,first_name); //zapis krstneho mena

    }

    //muzi
    else{
        //nahodny vyber krstneho mena
        rand_name=random_name(NUM_MEN_NAMES);
        strcpy(first_name,men_names[rand_name]);

        write_first_name(personal_data,first_name); //zapis krstneho mena

    }

    //nahodny vyber priezviska
    rand_name=random_name(NUM_LAST_NAMES);
    strcpy(last_name,last_names[rand_name]);

    write_last_name(personal_data, sex, last_name); //zapis priezviska

}



//generovanie pohlavia
void gen_sex(PersonalData *personal_data){

    int sex; //pohalvie 5>muz>0, zena=0

    sex=rand()%5;   //nahodne pohlavie

    choose_random_name(sex,personal_data);  //vybratie mena

}



//zapis osobne data
void write_personal_data(PersonalData *personal_data){

    write_birth_date(&(personal_data->birth_date));

    gen_sex(personal_data);
}



//vygeneruje zamestnanca
void generate_one_employee(Employee *employee, int num_employee, Employee all_employees[]){

    write_position(employee);   //zapise pracovnu poziciu

    write_ID(employee,num_employee,all_employees); //zapise ID potom co ho vygeneruje

    write_income(employee); //zapise mesacny plat

    write_personal_data(&(employee->personal_data));  //zapise osobne data

}



//zapise zamestnanca do pola
void write_employee(Employee *all_employees,int num_employee){

    Employee employee;

    generate_one_employee(&employee,num_employee, all_employees);

    *(all_employees+num_employee)=employee; //priradime zamestnanca do pola zamestnancov

}



//generuje zamestnancov
void generate_employees(Employee *all_employees, int num_emp){


    for (int loop=0; loop<num_emp; loop++){
        write_employee(all_employees,loop);
    }

    CLEAR_SCREEN();

    printf("%d zamestnancov bolo vygenerovanych.\n\n",num_emp);


}
