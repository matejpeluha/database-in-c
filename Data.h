//
// Created by Matej Peluha on 4/4/2019.
//

#ifndef ZADANIE8_DATA_H
#define ZADANIE8_DATA_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <windows.h>

#define MAX_GEN_EMPLOYEES 50
#define MAX_LENGTH_NAME 30
#define MAX_LENGTH_POSITION 23
#define CLEAR_SCREEN() system("cls")


//datum
typedef struct{
    int day;
    int month;
    int year;
}Date;

//osobne udaje, meno a datum narodenia
typedef struct{
    char name[MAX_LENGTH_NAME]; //meno
    Date birth_date;
}PersonalData;

//informacie o zamestnancovi
typedef struct{
    int ID; //ID cislo
    int income;   //plat
    char position[MAX_LENGTH_POSITION];   //pracovna pozicia
    PersonalData personal_data;  //osobne udaje
}Employee;

#endif //ZADANIE8_DATA_H
