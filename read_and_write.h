//
// Created by Matej Peluha on 4/20/2019.
//

#ifndef ZADANIE8_READ_AND_WRITE_H
#define ZADANIE8_READ_AND_WRITE_H

#include "using.h"

void write_database(FILE *fptr, int num_emp, Employee all_employees[]);
int database_is_empty(FILE *fptr);
int load_database(FILE *fptr, Employee *all_employees);
int number_of_employees(FILE *fptr);
#endif //ZADANIE8_READ_AND_WRITE_H
