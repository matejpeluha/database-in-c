//
// Created by Matej Peluha on 4/4/2019.
//

#ifndef ZADANIE8_GENERATE_H
#define ZADANIE8_GENERATE_H

#include "Data.h"

#define NUM_POSITIONS   6

#define CHANCE_MANAGER 5
#define CHANCE_WEBDESIGNER 25
#define CHANCE_ANALYTIC 45
#define CHANCE_DEVELOPER 75
#define CHANCE_TESTER 85
#define CHANCE_ADMIN 100

int get_number_employees();
int id_is_used(Employee all_employees[],int num_employees, int ID);
void generate_employees(Employee *all_employees, int num_emp);

#endif //ZADANIE8_GENERATE_H
