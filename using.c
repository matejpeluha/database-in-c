//
// Created by Matej Peluha on 4/5/2019.
//

#include "using.h"



//od uzivatela zoberie prikaz
void get_input(char *input){
    printf("Zadaj prikaz(pre vypis vsetkych prikazov zadaj 'help'):");
    scanf(" %[^\n]s",input);
    CLEAR_SCREEN();
}



//porovna stringy a vracia obratenu hodnotu kvoli if-om
int strings_are_same(char *input, char string[]){
    if (strcmp(input,string)==0) return 1; //pokial su rovnake, uzivatel zadal prikaz end a teda funkcia vracia 1
    else return 0;
}



//prikaz help
void help(){
    printf("ZAKLADNE PRIKAZY. Po ich zadani su k dispozicii dalsie VETVENE prikazy ktore da program neskor k dispozicii.\nVsetko pis bez medzier a apostrofov.");
    printf("\n0.prikaz: 'help' vypisuje vsetky zakaldne prikazy");
    printf("\n1.prikaz: 'show' zobrazi podla zadanych podmienok zamestnancov");
    printf("\n2.prikaz: 'sort' zoradi zamestnancov");
    printf("\n3.prikaz: 'add' umozni rucne pridat zamestnanca");
    printf("\n4.prikaz: 'change' umozni zmenit udaj zamestnanca ktory je v databaze");
    printf("\n5.Prikaz: 'delete' umozni vymazat zamestnanca");
    printf("\n6.prikaz: 'end' ukoncuje program\n\n");

}



//zistuje ci string je cislo, pokial je tak vrati 1
int string_is_number(char emp_to_show[], int len){

    int is_number=0;

    for (int loop=0;loop<len;loop++){
        is_number+=isdigit(emp_to_show[loop]); //pokial je cislica tak pripocita 1

    }

    if (is_number==len)return 1;

    else return 0;
}



//konvertuje string na cislo (mal by byt cislo)
int string_convert_to_number(char emp_to_show[],int len){
    int number=0;

    for (int loop=0;loop<len;loop++){
        number+=(((int)(emp_to_show[loop]))-'0')*((int)(pow(10,len-loop-1)));
    }

    return number;
}



//tvori hlavicku vypisu
void create_head(){

    printf("|  ID  |             MENO             |        POZICIA        | PLAT | DATUM NARODENIA |\n");
    printf(" --------------------------------------------------------------------------------------");

}


//printuje meno
void print_name(char name[], int len_name){


    int white_space=MAX_LENGTH_NAME-len_name-1;

    printf("  %s%*c",name,white_space,'|');

}



//printuje poziciu
void print_position(char position[], int len_position){

    int white_space=MAX_LENGTH_POSITION-len_position-1;

    printf("  %s%*c",position,white_space,'|');
}



//printuje den
void print_part_of_date(int part_of_date){
    if (part_of_date<10)    printf("0%d/",part_of_date);
    else printf("%d/",part_of_date);
}



//printuje datum narodenia
void print_date_birth(Date date){
    printf("|   ");

    //den
    print_part_of_date(date.day);

    //mesiac
    print_part_of_date(date.month);

    //rok
    printf("%d",date.year);

    printf("    |");
}



//printuje jedneho zamestnanca
void print_one_employee(Employee *employee){

    printf("\n| %d |",employee->ID);

    print_name(employee->personal_data.name, strlen(employee->personal_data.name));

    print_position(employee->position,strlen(employee->position));

    printf(" %d ",employee->income);

    print_date_birth(employee->personal_data.birth_date);

    printf("\n --------------------------------------------------------------------------------------");
}



//testuje ci selekcia podla platu prebehne
int test_income(int emp_income, int income, char symbol[]){
    if(strings_are_same(symbol,">") && emp_income>income) return 1;
    else if(strings_are_same(symbol,">=") && emp_income>=income) return 1;
    else if(strings_are_same(symbol,"<=") && emp_income<=income) return 1;
    else if(strings_are_same(symbol,"<") && emp_income<income) return 1;
    else if(strings_are_same(symbol,"==") && emp_income==income) return 1;
    else return 0;
}



//testuje ci selekcia podla datumu narodenia prebehne
int test_date(int day, int month, int year, char symbol[], Date date){
    if(strings_are_same(symbol,">") && ((date.year>year)|| (date.year==year&&date.month>month) || (date.year==year&&date.month==month&&date.day>day) )) return 1;
    else if(strings_are_same(symbol,">=") && ((date.year>year)|| (date.year==year&&date.month>month) || (date.year==year&&date.month==month&&date.day>=day) )) return 1;
    else if(strings_are_same(symbol,"<=") && ((date.year<year)|| (date.year==year&&date.month<month) || (date.year==year&&date.month==month&&date.day<=day) )) return 1;
    else if(strings_are_same(symbol,"<") && ((date.year<year)|| (date.year==year&&date.month<month) || (date.year==year&&date.month==month&&date.day<day) )) return 1;
    else if(strings_are_same(symbol,"==") && date.day==day && date.month==month && date.year==year ) return 1;
    else return 0;
}



//testuje ci mozeme vytlacit
int test_one_employee(Employee employee, char input[],char symbol[], int day, int month, int year, int income, char name_pos[]){

    int test;

    //pokial sme zadali selekciu podla platu
    if (strings_are_same(input,"plat")){
        test=test_income(employee.income,income,symbol);
    }

    //pokial sme zadali selekciu podla datumu
    else if(strings_are_same(input,"datum")){
        test=test_date(day,month,year,symbol, employee.personal_data.birth_date);
    }

    else if(strings_are_same(input,"meno")){
        test=strings_are_same(name_pos,employee.personal_data.name);
    }

    else if(strings_are_same(input,"pozicia")){
        test=strings_are_same(name_pos,employee.position);
    }

    else if(strings_are_same(input,"")) test=1;

    else test=0;


    return test;
}


//tlaci databazu
void print_database(Employee all_employees[], int num_emp_to_show, int num_emp, char input[], char symbol[], int day, int month, int year, int income, char find_pos_name[]){

    int actual_employee=0;  //ak je zadany pocet zamestnancov ktorych chceme tlacit mensi ako jeho celkovy pocet....premenna vhodna pri pouziti selekcie

    CLEAR_SCREEN();



        //vytvori hlavicku
        create_head();

        //printuje zamestnancov v zadanom pocte
        for (int loop = 0; loop < num_emp && actual_employee<num_emp_to_show; loop++) {

            //pokial zamestnanec vyhovia podmienkam selekcie
            if(test_one_employee(all_employees[loop],input,symbol,day,month,year,income,find_pos_name)==1) {

                print_one_employee(&(all_employees[loop])); //printuje konkretneho zamestnanca z pola zamestnancov

                actual_employee++;
            }
        }
        printf("\n\n");


}



//od uzivatela zisti mesiac
int get_month(){
    int month;
    char char_month[50];

    do {
        CLEAR_SCREEN();
        printf("Zadaj mesiac narodenia:");
        scanf(" %s", char_month);

        if(string_is_number(char_month,strlen(char_month))==0){
            month=SYNTAX_ERROR;
            continue;
        }

        month=string_convert_to_number(char_month,strlen(char_month));

    }while(month<0 || month>12);

    return month;
}



//od uzivatela zisti den
int get_day(int month, int year){
    int day;
    char char_day[50];

    do{
        CLEAR_SCREEN();

        printf("Zadaj den narodenia:");
        scanf(" %s",char_day);

        if (string_is_number(char_day,strlen(char_day))==0){
            day=SYNTAX_ERROR;
            continue;
        }

        day=string_convert_to_number(char_day,strlen(char_day));

    }while(((month==1 || month==3 || month==5 ||month ==7 || month==8 ||month==10 ||month==12)&&(day>31 || day<1)) ||
            ((month==2 || month==4 || month==6 ||month ==9|| month==11)&&(day>30 || day<1)) ||
            ((month==2)&&((day>28&&year%4!=0)||(day>29&&year%4==0)||(day<1))));


    return day;
}



//selekcia podla cisla
void selection_number(Employee all_employees[], int num_emp_to_show, int num_emp, char input[], char symbol[]){

    CLEAR_SCREEN();

    int day=-1, month=-1, year=-1, income=-1;

    char char_year[50], char_income[50];

    //pokial je zvoleny datum
    if (strings_are_same(input,"datum")){

        do {
            printf("Zadaj %s od ktoreho chces aby bol %s vsetkych zamestnancov ktorych vypise.",input,symbol);
            printf("\n\nZadaj rok narodenia:");
            scanf(" %s", char_year);

            CLEAR_SCREEN();

            if(string_is_number(char_year,strlen(char_year))){
                year=string_convert_to_number(char_year,strlen(char_year));
                break;
            }

        }while(1);

        month=get_month();

        day=get_day(month,year);
    }

    //pokial ide o plat
    else if(strings_are_same(input,"plat")){

        do {
            printf("Zadaj %s od ktoreho chces aby bol %s vsetkych zamestnancov ktorych vypise.",input,symbol);
            printf("\n\nPlat:");
            scanf(" %s", char_income);

            CLEAR_SCREEN();

            if(string_is_number(char_income,strlen(char_income))==0){
                income=SYNTAX_ERROR;
                continue;
            }

            income=string_convert_to_number(char_income,strlen(char_income));

        }while(income<1000);
    }

    print_database(all_employees,num_emp_to_show,num_emp,input,symbol,day,month,year,income,"");
}



//ziada od uzivatela zadat znamienka podla ktoreho seleckai prebehne
void equality(Employee all_employees[], int num_emp_to_show, int num_emp, char input[]){

    char symbol[5];

    printf("Zadal si selekciu podla faktora %s.",input);
    printf("Zadaj znamienko ne/rovnosti.\n '>' '<' '=' '<=' '>=' ");
    printf("\nPre zrusenie zadaj '1000'\n\nPrikaz: ");
    scanf("%s",symbol);

    if (strings_are_same(symbol,">") || strings_are_same(symbol,">=") || strings_are_same(symbol,"<") || strings_are_same(symbol,"<=") || strings_are_same(symbol,"=")) {
        selection_number(all_employees, num_emp_to_show, num_emp, input, symbol);
    }

    else if(strings_are_same(symbol,ESCAPE_STR)==0){
        CLEAR_SCREEN();
        printf("INVALID SYNTAX ERROR. Nespravne zadany prikaz. \n\n");
    }

    else if(strings_are_same(symbol,ESCAPE_STR)==1){
        CLEAR_SCREEN();
    }
}



//pyta string pre poziciu alebo meno v selekcii
void get_string_for_selection(Employee all_employees[], int num_emp_to_show, int num_emp, char input[]) {

    char find_string[MAX_LENGTH_NAME];

    CLEAR_SCREEN();

    if (strings_are_same(input, "pozicia")) {
        printf("Zadaj pozadovanu poziciu ktoru hladas.\n\n");
    } else {
        printf("Zadaj pozadovane meno ktore hladas.\n\n");
    }

    printf("Pre zrusenie zadaj '1000'.\n\n%s:", input);
    scanf(" %[^\n]s", find_string);


    if (strings_are_same(find_string, ESCAPE_STR) == 0) {
        print_database(all_employees, num_emp_to_show, num_emp, input, "", -1, -1, -1, -1, find_string);

    }

    else if(strings_are_same(find_string,ESCAPE_STR)==1){
        CLEAR_SCREEN();
    }
}



//zisti ci sme zadali faktor selektujuci podla cisla alebo stringu
void type_of_factor(Employee all_employees[], int num_emp_to_show, int num_emp, char input[]){

    CLEAR_SCREEN();

    //selekcia podla platu alebo datumu
    if (strings_are_same(input,"plat") || strings_are_same(input,"datum")){
        equality(all_employees,num_emp_to_show,num_emp,input);
    }

    //selekcia podla mena alebo pozicie
    else if (strings_are_same(input,"meno") || strings_are_same(input,"pozicia")){
        get_string_for_selection(all_employees, num_emp_to_show, num_emp, input);
    }

    else{
        printf("INVALID SYNTAX ERROR. Nedovoleny prikaz.\n\n");
    }

}



//selekcia where
void where_selection(Employee all_employees[], int num_emp_to_show, int num_emp){
    char input[8];

    CLEAR_SCREEN();

    printf("Zadaj faktor podla ktoreho prebehne selekcia. Zapis jeden z nasledujucich prikazov.");
    printf("\n 'plat' 'pozicia 'meno' datum'\n Pre zrusenie zadaj 1000");
    printf("\n\nZadany faktor:");
    scanf("%s",input);

    CLEAR_SCREEN();

    if(strings_are_same(input,ESCAPE_STR)==0) {
        type_of_factor(all_employees, num_emp_to_show, num_emp, input);
    }
}



//posledny prikaz 'do' alebo 'where' rozhodne ci budeme selektovat
void selection_of_employees(Employee all_employees[], int num_emp_to_show, int num_emp){

    char input[6];

    printf("VETVENE PRIKAZY: \n           'do' = vypis databazu bez selekcie\n           'where' = bude na vyber selekcia\n           '1000' = zrusenie");
    printf("\n\nZadaj prikaz:");
    scanf("%s",input);

    //zadany prikaz 'do'
    if (strings_are_same(input,"do")){
        print_database(all_employees,num_emp_to_show,num_emp,"","",-1,-1,-1,-1,"");
    }

    //zadany prikaz 'where'
    else if(strings_are_same(input,"where")){
        where_selection(all_employees,num_emp_to_show,num_emp);
    }

    //zadane nieco ine
    else if(strings_are_same(input,ESCAPE_STR)==0){
        CLEAR_SCREEN();
        printf("INVALID SYNTAX ERROR. Zadany prikaz neexistuje.\n\n");
    }

    else if(strings_are_same(input,ESCAPE_STR)==1) {
        CLEAR_SCREEN();
    }

}


//prikaz SHOW
void show(Employee all_employees[], int num_emp){

    char char_emp_to_show[50];
    int num_emp_to_show;

    printf("Zvolil si prikaz 'show', ktory zobrazuje zamestnancov.");
    printf("\nZapis pocet zamestnancov ktorych chces zobrazit. Pokial chces vsetkych zapis prikaz 'all.\n\nPre zrusenie zadaj kod 1000");
    printf("\n\nPocet zamestnancov: ");
    scanf("%s", char_emp_to_show);

    CLEAR_SCREEN();

    //pokial sme nacitali cislo
    if (string_is_number(char_emp_to_show,strlen(char_emp_to_show))==1){

        num_emp_to_show=string_convert_to_number(char_emp_to_show,strlen(char_emp_to_show));    //konverzia cisla zo stringu do integeru

        if ( num_emp_to_show<=num_emp && num_emp_to_show>0 ) {

            selection_of_employees(all_employees, num_emp_to_show, num_emp);
        }

            //pokial pocet zamestnancov je prilis velky
        else if(num_emp_to_show > num_emp && num_emp_to_show!=ESCAPE_NUM){
            printf("Prilis vela zadanych zamestnancov. Databaza aktualne obsahuje iba %d zamestnancov.\n\n",num_emp);
        }

        else if(num_emp_to_show<0){
            printf("Prilis malo zadanych zamestnancov. Dtabaza nevie vytlacit zaporny pocet zamestnancov.\n\n");
        }
    }

    //pokial sme nacitali slovo
    else{

        //pokial je string prikaz 'all'
        if (strings_are_same(char_emp_to_show,"all")){
            selection_of_employees(all_employees,num_emp,num_emp);  //printuje databazu
        }

        //pokial nie je string prikaz 'all'
        else{
            printf("INVALID SYNTAX ERROR. Zle zadany prikaz. Je mozne zapisat len cislo alebo prikaz 'all'\n\n.");
        }

    }


}



//zvolenie orientacie ci sortujeme od najmensieho alebo najvacsieho
void get_orientation_sort(char *symbol){

    char input[2];

    printf("Zadaj znamienko '+' pre zoradenie VZOSTUPNE a '-' pre zoradenie od ZOSTUPNE.\n\nPre zrusenie zadaj 1000\n\nZnamienko:");
    scanf("%s",input);

    CLEAR_SCREEN();

    if (strings_are_same(input,"+") || strings_are_same(input,"-")){
        strcpy(symbol,input);
    }

    else if(strings_are_same(input,ESCAPE_STR)==0){
        printf("INVALID SYNTAX ERROR. Zadany prikaz je nespravny\n\n");
    }

}



//ziskaj faktor podla ktoreho chceme sortovat
void get_factor_sort(char *factor, char *symbol){
    char input[8];

    CLEAR_SCREEN();

    printf("Zadaj faktor podla ktoreho chces zoradit databazu. \n    'plat' 'meno' 'pozicia' 'datum'\n\nPre zrusenie zadaj 1000");
    printf("\n\nPrikaz:");
    scanf(" %[^\n]s",input);

    CLEAR_SCREEN();

    if (strings_are_same(input,"plat") || strings_are_same(input,"meno") || strings_are_same(input,"pozicia") || strings_are_same(input,"datum")){
        strcpy(factor,input);
        get_orientation_sort(symbol);
    }

    else if(strings_are_same(input,ESCAPE_STR)==0){
        printf("INVALID SYNTAX ERROR. Nespravne zadany prikaz.\n\n");
    }
}



//porovna platy
int compare_income(int income1, int income2, char symbol[]){

    //vzostupne
    if (strings_are_same(symbol,"+")){
        if (income1>income2) return 1;
        else return 0;
    }

    //zostupne
    else if(strings_are_same(symbol,"-")){
        if (income1<income2) return 1;
        else return 0;
    }

    else return 0;
}



//porovna datumy
int compare_date(int day1,int month1, int year1, int day2, int month2, int year2, char symbol[]){

    if (strings_are_same(symbol,"+")){
        if ((year1>year2) || (year1==year2 && month1>month2) || (year1==year2 && month1==month2 && day1>day2)) return 1;
        else return 0;
    }

    else if (strings_are_same(symbol,"-")){
        if ((year1<year2) || (year1==year2 && month1<month2) || (year1==year2 && month1==month2 && day1<day2)) return 1;
        else return 0;
    }

    else return 0;
}



//porovna stringy abecedne ktory je vacsi/mensi (FUJ KOD)
int compare_strings_by_alphabet(char string1[],char string2[], int len_string1, char symbol[]){

    int test=0,loop=0;

    //vzostupne
    if(strings_are_same(symbol,"+")) {

        //kontroulujeme pismena v stringu a porovnavame, pokial sa rovnaju tak ideme na dalsie dalsie a dalsie
        for (loop=0; loop < len_string1; loop++) {
            if ((int)(tolower(string1[loop])) > (int)(tolower(string2[loop]))) {
                test++;
                break;
            }

            else if((int)(tolower(string1[loop])) < (int)(tolower(string2[loop]))){
                break;
            }
        }
    }

    //zostupne
    else if (strings_are_same(symbol,"-")){

        //kontroulujeme pismena v stringu a porovnavame, pokial sa rovnaju tak ideme na dalsie dalsie a dalsie
        for (loop=0; loop < len_string1; loop++) {
            if ((int)(tolower(string1[loop])) < (int)(tolower(string2[loop]))) {
                test++;
                break;
            }
            else if((int)(tolower(string1[loop])) > (int)(tolower(string2[loop]))){
                break;
            }
        }
    }


    return test;

}



//porovnaj dvoch zamestnancov podla faktora
int compare(Employee employee1, Employee employee2, char factor[], char symbol[]){

    //porovna platy
    if (strings_are_same(factor,"plat")){
        return compare_income(employee1.income,employee2.income,symbol);
    }

    //porovna datumy
    else if(strings_are_same(factor,"datum")){
        return compare_date(employee1.personal_data.birth_date.day,
                employee1.personal_data.birth_date.month,
                employee1.personal_data.birth_date.year,
                employee2.personal_data.birth_date.day,
                employee2.personal_data.birth_date.month,
                employee2.personal_data.birth_date.year,
                symbol);
    }

    else if(strings_are_same(factor,"meno")){
        return compare_strings_by_alphabet(employee1.personal_data.name,employee2.personal_data.name,strlen(employee1.personal_data.name), symbol);
    }

    else if(strings_are_same(factor,"pozicia")){
        return compare_strings_by_alphabet(employee1.position,employee2.position,strlen(employee1.position),symbol);
    }


}



//bubble sort nam zoradi zoznam podla poziadavky
void bubble_sort(Employee *all_employees, int num_emp, char factor[], char symbol[], char *last_factor, char *last_symbol){

    Employee temp_employee;

    for (int i = 0; i < num_emp - 1; i++) {
        for (int j = 0; j < num_emp - i - 1; j++) {
            if (compare(all_employees[j], all_employees[j + 1], factor, symbol) == 1) {
                temp_employee = all_employees[j];
                *(all_employees + j) = *(all_employees + j + 1);
                *(all_employees + j + 1) = temp_employee;
            }
        }
    }

    //ulozi co bolo pouzite posledne
    strcpy(last_factor,factor);
    strcpy(last_symbol,symbol);

}




//prikaz SORT
void sort(Employee *all_employees, int num_emp, FILE *fptr, char *last_factor, char *last_symbol){

    char factor[8]="";
    char symbol[2]="";

    //ziskaj faktor podla ktoreho chceme sortovat
    get_factor_sort(factor,symbol);

    //pokial sme spravne nacitali faktor podla ktoreho sortujeme a pokial sme spravne nacitali znamienka orientacie sortovania
    if (strings_are_same(factor,"")==0 && strings_are_same(symbol,"")==0){
        bubble_sort(all_employees, num_emp, factor, symbol, last_factor, last_symbol);
    }

    //zapise do .txt
    write_database(fptr, num_emp, all_employees);
}



//uzivatel vytvori meno zamestnancovi
void get_name(Employee *employee){

    char name[MAX_LENGTH_NAME];

    do {
        CLEAR_SCREEN();
        printf("Zapis meno zamestnanca(max dlazka %d):",MAX_LENGTH_NAME);
        scanf(" %[^\n]s",name);
    }while(strlen(name)>MAX_LENGTH_NAME);

    strcpy(employee->personal_data.name,name);
}



//uzivatel vytvori ID zamestnancovi
void get_ID(Employee *employee, Employee *all_employees, int num_employees){

    int ID;
    char charID[5];

    CLEAR_SCREEN();

    do{

        printf("Zadaj ID zamestnanca(rozsah5000-9999):");
        scanf("%s",charID);

        CLEAR_SCREEN();

        if(string_is_number(charID,strlen(charID))==0){
            ID=SYNTAX_ERROR;
            continue;
        }

        ID=string_convert_to_number(charID,strlen(charID));


        if(id_is_used(all_employees, num_employees, ID)==1){
            printf("ID uz je obsadene. ID musi byt unikatne.\n\n");
        }

    }while(id_is_used(all_employees, num_employees, ID)==1 || (ID<5000 || ID >9999));

    employee->ID=ID;
}



//uzivatel urci zamestnancovi poziciu
void get_position(Employee *employee){

    char position[MAX_LENGTH_POSITION];

    do {
        CLEAR_SCREEN();
        printf("Zapis poziciu zamestnanca(max dlazka %d):",MAX_LENGTH_POSITION-2);
        scanf(" %[^\n]s",position);
    }while(strlen(position)>MAX_LENGTH_POSITION-2);

    strcpy(employee->position,position);
}



//uzivatel urci zamestnancov plat
void get_income(Employee *employee){

    int income;
    char char_income[50];

    CLEAR_SCREEN();

    do{
        printf("Zadaj mzdu zamestnancovi. \nminimalna: %d\nmaximalna: %d\n Mzda:",MINIMUM_SALARY,9999);
        scanf(" %s",char_income);

        CLEAR_SCREEN();

        if(string_is_number(char_income,strlen(char_income))==0){
            income=SYNTAX_ERROR;
            continue;
        }

        income=string_convert_to_number(char_income,strlen(char_income));

    }while(income<MINIMUM_SALARY || income>9999);

    employee->income=income;
}



//uzivatel urci zamestnancov datum narodenia
void get_birth_date(Date *date){

    char char_year[5];
    int day,month,year;

    CLEAR_SCREEN();
    do {

        printf("najstarsi mozny rok: 1930\nnajmladsi mozny rok: 2001");
        printf("\n\nZadaj rok narodenia zamestnanca:");
        scanf(" %s", char_year);

        CLEAR_SCREEN();

        if(string_is_number(char_year,strlen(char_year))==0){
            year=SYNTAX_ERROR;
            continue;
        }

        year = string_convert_to_number(char_year, strlen(char_year));


    }while(year<1930 || year>2001);

    month=get_month();

    day=get_day(month,year);

    date->year=year;
    date->month=month;
    date->day=day;

}



//manualne vytvorenie zamestnanca
void create_employee(Employee *employee, Employee *all_employees, int num_employees){

    get_name(employee);

    get_ID(employee, all_employees, num_employees);

    get_position(employee);

    get_income(employee);

    get_birth_date(&(employee->personal_data.birth_date));

    CLEAR_SCREEN();


}



//prikaz ADD
Employee* add(Employee *all_employees, int *num_emp, FILE *fptr, char *last_factor, char *last_symbol){

    Employee employee;

    CLEAR_SCREEN();

    //vytvori zamestnanca
    create_employee(&employee, all_employees, *num_emp);

    //zvacsenie pola
    all_employees=(Employee*) realloc(all_employees,((*num_emp)+1)* sizeof(Employee));

    //dosadi zamestnanca
    *(all_employees + *num_emp) = employee;

    //zvacsi pocet zamestnancov
    (*num_emp)++;

    //zorad nech je na spravnom mieste podla posledneho zoradenia
    if (strings_are_same(last_symbol,"")==0 && strings_are_same(last_factor,"")==0) {
        bubble_sort(all_employees, *num_emp, last_factor, last_symbol, last_factor, last_symbol);
    }

    //zapise do .txt suboru
    write_database(fptr,*num_emp, all_employees);

    return all_employees;
}



//najde zamestnanca s danym ID
int find_employee_by_ID(Employee *all_employees, int ID){

    int num;

    for (num=0; ID != all_employees[num].ID;num++);

    return num;

}



//vypyta od uzivatela co chce zmenit
void get_change_factor(Employee *employee){

    char factor[8];

    CLEAR_SCREEN();

    printf("Zadaj faktor ktory chces zmenit.\n 'plat' 'meno' 'pozicia' datum'\n\nPre zrusenie zadaj 1000");
    printf("\n\nPrikaz:");
    scanf(" %s",factor);

    if (strings_are_same(factor,"plat")){
        get_income(employee);
    }

    else if(strings_are_same(factor,"meno")){
        get_name(employee);
    }

    else if(strings_are_same(factor,"pozicia")){
        get_position(employee);
    }

    else if(strings_are_same(factor,"datum")){
        get_birth_date(&(employee->personal_data.birth_date));
    }

    else if(strings_are_same(factor,ESCAPE_STR)==0);


    else{
        printf("INVALID SYNTAX ERRO. Zle zadany prikaz.\n");
    }


}



//pomocka uzivatelovi pri meneni a mazani
void help_show_database(Employee *all_employees,int num_emp, char input[]){
    if(strings_are_same(input,"0")){
        printf("\n");
        print_database(all_employees,num_emp,num_emp,"","",-1,-1,-1,-1,"");
        printf("\n");
    }
}



//ziskaj od uzivatela existujuce ID
int get_existing_ID(Employee *all_employees,int num_emp){

    char charID[50];
    int intID;

    //dokola si pytaj  ID kym uzivatel nezada uz pouzite
    do {

        printf("Najdi zamestnanca podla unikatneho ID.\nPre zrusenie zadaj '1000'\nPre zobrazenie vsetkych zamestnancov zadaj '0'\n\n");
        printf("Zadaj ID(alebo %d alebo 0):",ESCAPE_NUM);
        scanf(" %s",charID);

        CLEAR_SCREEN();

        //pokial sme nezadali cislo
        if(string_is_number(charID,strlen(charID))==0){
            intID=SYNTAX_ERROR;
            continue;
        }

        //konverzia na cislo
        intID = string_convert_to_number(charID, strlen(charID));

        //zobrazenie datavazy
        help_show_database(all_employees,num_emp, charID);

    }while(id_is_used(all_employees,num_emp,intID)==0 && intID!=ESCAPE_NUM);

    return intID;
}



//prikaz CHANGE
void change_employee(Employee *all_employees, int num_emp, FILE *fptr, char *last_factor, char *last_symbol){

    int ID;
    int employee;

    CLEAR_SCREEN();

    printf("Zvolil si prikaz change.\n");
    //ziska od uzivatela ID, bude pytat stale kym nedostane uz existujuce
    ID=get_existing_ID(all_employees,num_emp);

    //neunikova sekvencia
    if(ID!=ESCAPE_NUM && ID !=SYNTAX_ERROR ){
        employee=find_employee_by_ID(all_employees,ID);   //ulozi ktory zamestnanec ma dane ID
        get_change_factor(all_employees+employee);

        //zorad nech je na spravnom mieste podla posledneho zoradenia
        if (strings_are_same(last_symbol,"")==0 && strings_are_same(last_factor,"")==0) {
            bubble_sort(all_employees, num_emp, last_factor, last_symbol, last_factor, last_symbol);
        }

        write_database(fptr,num_emp,all_employees);
    }

    CLEAR_SCREEN();

}



//posunie zoznam do lava od zamestnanca ktoreho mazeme
Employee* move_elements_of_array(Employee *all_employees,int employee, int *num_emp){

    //posun zamestnancov do lava
    for(int loop=employee;loop<(*num_emp)-1;loop++){
        *(all_employees+loop)=all_employees[loop+1];
    }

    (*num_emp)--;//zmensenie poctu zamestnancov

    all_employees=(Employee*) realloc(all_employees,(*num_emp)*sizeof(Employee));   //zmensenie pola

    return all_employees;
}



//ziska od uzivateal faktor podla ktoreho bude mazat
void get_factor_delete(char *factor){
    printf("Zapis faktor podla ktoreho budeme mazat.\n'id' 'meno' 'plat' 'pozicia' 'datum'\n'1000' pre zrusenie\n\nFaktor:");
    scanf("%s",factor);
}



//odstranuje zamestnancov na zaklade mena
Employee* delete_by_name( Employee *all_employees, int *num_emp){

    char name[MAX_LENGTH_NAME];

    Employee employee;

    get_name(&employee);

    //get_name uklada do struktury tak to prehodime do name
    strcpy(name,employee.personal_data.name);

    //prechadzame zamestnancov
    for(int loop=0;loop<*num_emp;loop++){

        //ak maju rovnake meno ako je zadane
        if (strings_are_same(all_employees[loop].personal_data.name,name)){

            all_employees=move_elements_of_array(all_employees,loop,num_emp); //posun zamestnancov na vymazanie daneho

            loop--; //potrebujeme otestovat aj toho zamestnanca ktory sa posunulu na poziciu loop
        }

    }
    CLEAR_SCREEN();

    return all_employees;
}



//mazanie podla pozicie
Employee* delete_by_position( Employee *all_employees, int *num_emp){
    char position[MAX_LENGTH_POSITION];
    Employee employee;

    get_position(&employee);

    //get_position uklada do struktury tak to prehodime do premennej position
    strcpy(position,employee.position);

    //prechadzame zamestnancov
    for(int loop=0;loop<*num_emp;loop++){

        //ak maju rovnake meno ako je zadane
        if (strings_are_same(all_employees[loop].position,position)){

            all_employees=move_elements_of_array(all_employees,loop,num_emp); //posun zamestnancov na vymazanie daneho

            loop--; //potrebujeme otestovat aj toho zamestnanca ktory sa posunul na poziciu loop
        }

    }
    CLEAR_SCREEN();

    return all_employees;
}



//porovnavat a vymazat
Employee* delete_by_income_in_compare(int *num_emp,char *symbol, Employee *all_employees){

    Employee employee;
    int income;

    get_income(&employee);

    //get_income uklada plat do employee tak to prehodime do income
    income=employee.income;

    //prechadza zamestanncov
    for (int loop=0;loop<*num_emp;loop++){

        //testuje zadane parametre
        if (test_income((all_employees+loop)->income,income,symbol)){

            all_employees=move_elements_of_array(all_employees,loop,num_emp); //posun zamestnancov na vymazanie daneho

            loop--; //potrebujeme otestovat aj toho zamestnanca ktory sa posunul na poziciu loop
        }
    }

    CLEAR_SCREEN();

    return all_employees;
}



//mazanie podla platu
Employee* delete_by_income(Employee *all_employees, int *num_emp){

    char symbol[50];

    printf("Zadaj znamienko ne/rovnosti.\n '>' '<' '=' '<=' '>=' ");
    printf("\nPre zrusenie zadaj '1000'\n\nPrikaz: ");
    scanf("%s",symbol);

    CLEAR_SCREEN();
    if (strings_are_same(symbol,">") || strings_are_same(symbol,">=") || strings_are_same(symbol,"<") || strings_are_same(symbol,"<=") || strings_are_same(symbol,"=")) {
        all_employees=delete_by_income_in_compare(num_emp,symbol,all_employees);
    }

    else if(strings_are_same(symbol,ESCAPE_STR)==0){
        CLEAR_SCREEN();
        printf("INVALID SYNTAX ERROR. Nespravne zadany prikaz. \n\n");
    }

    else if(strings_are_same(symbol,ESCAPE_STR)==1){
        CLEAR_SCREEN();
    }

    return all_employees;
}



//mazanie podla datumu pri porovnavani
Employee* delete_by_date_in_compare(int *num_emp,char *symbol, Employee *all_employees){
    Employee employee;
    int day, month, year;

    get_birth_date(&(employee.personal_data.birth_date));

    //get_birth_date uklada plat do employee tak to prehodime do income
    day=employee.personal_data.birth_date.day;
    month=employee.personal_data.birth_date.month;
    year=employee.personal_data.birth_date.year;


    //prechadza zamestanncov
    for (int loop=0;loop<*num_emp;loop++){

        //testuje zadane parametre
        if (test_date(day, month, year, symbol, (all_employees+loop)->personal_data.birth_date)){

            all_employees=move_elements_of_array(all_employees,loop,num_emp); //posun zamestnancov na vymazanie daneho

            loop--; //potrebujeme otestovat aj toho zamestnanca ktory sa posunul na poziciu loop
        }
    }

    CLEAR_SCREEN();

    return all_employees;
}



//mazanie podla datumu narodenia
Employee* delete_by_date(Employee *all_employees,int *num_emp){
    char symbol[50];

    printf("Zadaj znamienko ne/rovnosti.\n '>' '<' '=' '<=' '>=' ");
    printf("\nPre zrusenie zadaj '1000'\n\nPrikaz: ");
    scanf("%s",symbol);

    CLEAR_SCREEN();
    if (strings_are_same(symbol,">") || strings_are_same(symbol,">=") || strings_are_same(symbol,"<") || strings_are_same(symbol,"<=") || strings_are_same(symbol,"=")) {
        all_employees=delete_by_date_in_compare(num_emp,symbol,all_employees);
    }

    else if(strings_are_same(symbol,ESCAPE_STR)==0){
        CLEAR_SCREEN();
        printf("INVALID SYNTAX ERROR. Nespravne zadany prikaz. \n\n");
    }

    else if(strings_are_same(symbol,ESCAPE_STR)==1){
        CLEAR_SCREEN();
    }

    return all_employees;
}



//prikaz na mazanie podla ID
Employee* delete_by_ID(Employee *all_employees, int *num_emp){

    int ID;

    ID=get_existing_ID(all_employees,*num_emp);

    for (int loop=0;loop<*num_emp;loop++){

        if((all_employees+loop)->ID==ID){
            all_employees=move_elements_of_array(all_employees,loop,num_emp);
        }
    }

    return all_employees;
}



//prikaz DELETE
Employee* delete_employee(Employee *all_employees,int *num_emp, FILE *fptr){
    char factor[15];

    CLEAR_SCREEN();
    printf("Zvolil si prikaz change.\n");

    get_factor_delete(factor);

    if(strings_are_same(factor,"meno")){
        CLEAR_SCREEN();
        all_employees=delete_by_name(all_employees,num_emp);
        write_database(fptr, *num_emp, all_employees);
        CLEAR_SCREEN();
    }

    else if(strings_are_same(factor,"pozicia")){
        CLEAR_SCREEN();
        all_employees=delete_by_position(all_employees,num_emp);
        write_database(fptr, *num_emp, all_employees);
        CLEAR_SCREEN();
    }

    else if(strings_are_same(factor,"plat")){
        CLEAR_SCREEN();
        all_employees=delete_by_income(all_employees,num_emp);
        write_database(fptr, *num_emp, all_employees);
        CLEAR_SCREEN();
    }

    else if(strings_are_same(factor,"datum")){
        CLEAR_SCREEN();
        all_employees=delete_by_date(all_employees,num_emp);
        write_database(fptr, *num_emp, all_employees);
        CLEAR_SCREEN();
    }

    else if(strings_are_same(factor,"id")){
        CLEAR_SCREEN();
        all_employees=delete_by_ID(all_employees,num_emp);
        write_database(fptr,*num_emp,all_employees);
    }

    else if(strings_are_same(factor,ESCAPE_STR)) {
        CLEAR_SCREEN();
    }

    else{
        CLEAR_SCREEN();
        printf("INVALID SYNTAX ERROR.\n");
    }


    return all_employees;
}



//hlavna funkcia pre pouzivanie prikazov v databaze
void use_database(Employee *all_employees, int num_emp, FILE *fptr){

    char input[MAX_INPUT_SIZE];
    char last_factor[10]="", last_symbol[10]="";

    //ziskanie inputu od uzivatela
    get_input(input);


    //po zadani prikazu 'end' program skonci
    while(strings_are_same(input,END)==0){

        //prikaz help
        if (strings_are_same(input,HELP)){
            help();
        }

        //prikaz 'show'
        else if(strings_are_same(input,SHOW)){
            show(all_employees, num_emp);
        }

        //prikaz sort
        else if(strings_are_same(input,SORT)){
            sort(all_employees,num_emp, fptr, last_factor, last_symbol);
        }

        //prikaz add
        else if(strings_are_same(input,ADD)){
            all_employees=add(all_employees,&num_emp, fptr, last_factor, last_symbol);
        }

        //prikaz change
        else if(strings_are_same(input,CHANGE)){
            change_employee(all_employees,num_emp,fptr,last_factor,last_symbol);
        }

        //prikaz DELETE (doladit mazanie posledneho zamestnanca po posune, ktory tam ostane realne dvakrat aj ked na vypise je raz)
        else if(strings_are_same(input,DELETING)){
            all_employees=delete_employee(all_employees,&num_emp,fptr);
        }

        //zle zadany prikaz
        else{
            printf("INVALID SYNTAX ERROR. Zle zadany prikaz. Pre vypis moznych prikazov zadaj prikaz 'help'.\n\n");
        }

        //ziskanie input od uzivatela
        get_input(input);


    }

}
