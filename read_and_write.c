//
// Created by Matej Peluha on 4/20/2019.
//

#include "read_and_write.h"



//otestuje ci je subor prazdny, ak ano vrati 1
int database_is_empty(FILE *fptr){

    long size;

    fseek (fptr, 0, SEEK_END);  //prejdi nakoniec
    size = ftell(fptr); //pocet zapisanych bajtov
    fclose(fptr);

    if (size==0) return 1;
    else return 0;

}



//zapise databazu do txt suboru
void write_database(FILE *fptr, int num_emp, Employee all_employees[]){

    fptr=fopen("database.txt","w");    //subor v ktorom je databaza otvorime

    for (int loop=0;loop<num_emp;loop++){

        fprintf(fptr,"%d\n%s\n%d\n%s\n%d\n%d\n%d\n\n",all_employees[loop].ID,
                all_employees[loop].personal_data.name,
                all_employees[loop].income,
                all_employees[loop].position,
                all_employees[loop].personal_data.birth_date.year,
                all_employees[loop].personal_data.birth_date.month,
                all_employees[loop].personal_data.birth_date.day);

    }

    fclose(fptr);

}



//zisti pcet riadkov
int number_of_employees(FILE *fptr){

    int lines=0;
    int num_emp=0;
    char letter;

    fptr=fopen("database.txt","r");

    //testuje kazdy char
    while((letter=fgetc(fptr))!=EOF){

        //ked je '\n' ideme na dalsi riadok
        if(letter=='\n'){
            lines++;
        }
    }

    fclose(fptr);

    num_emp=lines/8;

    return num_emp;
}



//zapise rok
void save_year(Date *date, int year){

    date->year=year;

}



//zapise mesiac
int save_month(Date *date, int month){

    if (month>=1 && month<=12) {
        date->month = month;
        return 0;
    }

    else {
        printf("V databaze sa nachadza neexistujuci datum.\n\n");
        return 1;
    }
}



//zapisuje den
int save_day(Date *date,int year, int month, int day){

    if (((month==1 || month==3 | month==5 || month==7 || month==8 || month==10 || month==12)&& day<=31 && day>=1) ||
        ((month==4 | month==6 || month==9 || month==11 ) && day>=1 && day <=30) ||
        (month==2 && ((year%4==0 && day>=1 && day<=29)||(year%4>0 && day>=1 && day<=28)))){

        date->day=day;
        return 0;
    }

    else{
        printf("V databaze sa nachadza neexistujuci datum.\n\n");
        return 1;
    }
}



//zapisuje a kontroluje datum
int save_date(Employee *actual_employee, char one_info[], int num_info){

    int year, month, day;
    int error=1;

    //rok
    if (num_info==4){

        year=string_convert_to_number(one_info,strlen(one_info));   //prekonvertuje rok zo stringu na cislo

        save_year(&(actual_employee->personal_data.birth_date),year);//zapise rok

        error=0;


    }

    //mesiac
    else if (num_info==5){

        month=string_convert_to_number(one_info,strlen(one_info));

        error=save_month(&(actual_employee->personal_data.birth_date),month);

    }

    //den
    else if(num_info==6){

        day=string_convert_to_number(one_info,strlen(one_info));
        month=actual_employee->personal_data.birth_date.month;
        year=actual_employee->personal_data.birth_date.year;

        error=save_day(&(actual_employee->personal_data.birth_date),year, month, day);

    }

    return error;
}



//zapisal meno
int save_ID(Employee *employee, int ID){

    if (ID>=1000 && ID<=9999) {
        employee->ID = ID;
        return 0;
    }

    else{
        printf("Nevhodne ID. Povolene rozpatie 5000-9999\n");
        return 1;
    }
}



//zapise meno
int save_name(Employee *employee, char name[], int len_name){

    if (len_name<MAX_LENGTH_NAME) {
        strcpy(employee->personal_data.name, name);
        return 0;
    }

    else {
        printf("Prilis dlhe meno.\n");
        return 1;
    }
}



//zapise plat
int save_income(Employee *employee, int income){

    if (income>0 && income<=9999) {
        employee->income = income;
        return 0;
    }

    else{
        printf("Nepovolena vyska platu.");
    }
}



//zapise poziciu
int save_position(Employee *employee, char position[], int len_pos){
    if(len_pos<MAX_LENGTH_POSITION) {
        strcpy(employee->position, position);
        return 0;
    }

    else {
        printf("Prilis dlhy nazov pracovnej pozicie.\n");
        return 1;
    }
}



//zapise konkretnu jednu informaciu
int save_one_info(Employee *actual_employee, char one_info[], int num_info){

    int error=1;
    int ID,income;

    //ID
    if (num_info==0 && string_is_number(one_info,strlen(one_info))){

        ID=string_convert_to_number(one_info,strlen(one_info));

        error=save_ID(actual_employee,ID);
    }

    //meno
    else if(num_info==1 && string_is_number(one_info,strlen(one_info))==0){

        error=save_name(actual_employee,one_info,strlen(one_info));

    }

    //plat
    else if (num_info==2 && string_is_number(one_info,strlen(one_info))){

        income=string_convert_to_number(one_info,strlen(one_info));

        error=save_income(actual_employee,income);
    }

    //pozicia
    else if(num_info==3 && string_is_number(one_info,strlen(one_info))==0){

        error=save_position(actual_employee,one_info,strlen(one_info));

    }

    //datum narodenia
    else if ((num_info>=4 && num_info<=6)  && string_is_number(one_info,strlen(one_info))){

        error=save_date(actual_employee,one_info,num_info);


    }

    //prazdny riadok pred prechodom k dalsiemu zamestnancovi
    else if(num_info==7 && strings_are_same(one_info,"")){
        error=0;
    }

    return error;

}



//vyhodnoti ci nastal error a v ktorom riadku
int is_not_error(int line, int error){
    //ak nastal error, zobraz na 50000 milisekund chybovu hlasku
    if(error==1) {
        printf("Chyba v .txt subore databazy. Riadok: %d",line);
        Sleep(5000);
        return 0;
    }

    else return 1;
}



//nacita do pola zamestanncov z databazy
int load_database(FILE *fptr, Employee *all_employees){

    fptr=fopen("database.txt", "r");

    char buff;  //pomocny buffer ktory nacitava znaky

    int letter=0;   //znaci pri ktorom sme pismenku

    int actual_employee=0;  //aktualny zamestnanec

    int num_info=0;     //poradie info

    char info[500]="";   //dana informacia v riadku

    int error=0;

    int line=0;


    //nacitava znak za znakom
    while((buff=fgetc(fptr))!=EOF && is_not_error(line,error)){

        //pokial sme na konci riadku, mame 1 info o zamestnancovi
        if (buff=='\n') {

            line++;

            info[letter]='\0';  //ukoncime string s informaciami

            error=save_one_info(all_employees+actual_employee, info, num_info);   //zapise zamestnanca do zoznam

            num_info++;

            //po ziskan vsetkych informacii o zamestnancovi
            if(num_info==8) {

                num_info=0;

                actual_employee++;

            }
            //ideme na novy riadok
            strcpy(info,"");
            letter=0;

            continue;
        }

        info[letter]=buff;
        letter++;
    }


    fclose(fptr);

    return error;
}
